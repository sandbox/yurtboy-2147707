(function ($) {
    Drupal.behaviors.behat_editor_delete = {
        attach: function (context) {
            var token = Drupal.behat_editor.get_token();
            var data = {};
            $('a.delete').click(function(e){
                $('#beModal').modal();
                e.preventDefault();
            });

            $('button.confirm-delete').click(function(e){
                $('#beModal').modal('hide');
                var url = $('a.delete').attr('href');
                var parameters = {};
                var data = Drupal.behat_editor.action('DELETE', token, parameters, url);
                var filename = Drupal.behat_editor.split_filename($('input[name=filename]').val());
                if(data.error == 0) {
                    window.location.replace("/admin/behat/index");
                }
                Drupal.behat_editor.renderMessage(data);
            });
        }
    };
})(jQuery);